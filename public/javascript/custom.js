$(document).ready(function() {
    var editLink = $('.Student_Edit');
    var removeLink = $('.Student_Remove');
    var studentIDField = $('#Form_RenderForm_ID');
    var studentNameField = $('#Form_RenderForm_Name');
    var studentPhoneField = $('#Form_RenderForm_Phone');
    var studentAddressField = $('#Form_RenderForm_Address');
    var stateField = $('#Form_RenderForm_State');
    var lessonsCheckBox = $('input:checkbox');
    var provinceOptions = $('#Form_RenderForm_ProvinceID');
    var cityOptions = $('#Form_RenderForm_CityID');
    // var lessonsCheckBox = $('input:checkbox[name^="Lessons"]');
    var clearButton = $('#Form_RenderForm_action_clear');

    studentAddressField.text(null);

    editLink.click(function(event) {
        event.preventDefault();

        var id = $(this).data('id');
        var studentName = $(this).data('name');
        var studentPhone = $(this).data('phone');
        var studentAddress = $(this).data('address');
        var studentClassroom = $(this).data('classroom');
        var studentLessons = $(this).data('lessons');
        var studentProvince = $(this).data('province');
        var studentCity = $(this).data('city');

        console.log('ID: '+id);
        console.log('Name: '+studentName);
        console.log('Phone: '+studentPhone);
        console.log('Classroom: '+studentClassroom);

        studentIDField.val(id);
        studentNameField.val(studentName);
        studentPhoneField.val(studentPhone);
        selectOption('#Form_RenderForm_ClassroomID', studentClassroom);
        selectOption('#Form_RenderForm_ProvinceID', studentProvince);
        selectOption('#Form_RenderForm_CityID', studentCity);
        checkAnyBox(studentLessons);

        studentAddressField.text(studentAddress);
        stateField.val('update');
    });

    clearButton.click(function () {
        studentIDField.val('');
        studentNameField.val('');
        studentPhoneField.val('');
        selectOption(1);
        uncheckAnyBox(lessonsCheckBox);
        studentAddressField.text('');
        stateField.val('insert');
    });

    removeLink.click(function() {
        return confirm('Anda yakin ingin menghapus data ini?');
    });

    provinceOptions.change(function() {
        var id = $(this).val();
        console.log('Province ID: '+id+' is selected');
        getCityByProvinceID(id);
    });

    // Pagination
    if ($('.pagination').length) {
        var paginate = function (url) {
            var param = '&ajax=1',
                ajaxUrl = (url.indexOf(param) === -1) ?
                           url + '&ajax=1' :
                           url,
                cleanUrl = url.replace(new RegExp(param+'$'),'');

            $.ajax(ajaxUrl)
                .done(function (response) {
                    $('.container .main').html(response);
                    $('html, body').animate({
                        scrollTop: $('.container .main').offset().top
                    });
                    window.history.pushState(
                        {url: cleanUrl},
                        document.title,
                        cleanUrl
                    );
                })
                .fail(function (xhr) {
                    alert('Error: ' + xhr.responseText);
                });

        };

        $('.pagination a').click(function (e) {
            $('.Edit_Link').click(function (event) {
                event.preventDefault();
            });

            e.preventDefault();
            var url = $(this).attr('href');
            paginate(url);
        });

        window.onpopstate = function(e) {
            if (e.state.url) {
                paginate(e.state.url);
            } else {
                e.preventDefault();
            }
        };
    }

    function selectOption(element, i) {
        return $(element + ' option[value="' + i + '"]').prop('selected', true);
    }

    function checkAnyBox(values) {
        // console.log(values.length);
        // array = JSON.parse(values+'');
        values = values + '';

        var array = values.split(', ');
        $.each(array, function(index, value) {
            console.log(index + ': ' + value);   // alerts 0:[1 ,  and  1:2]
        });

        // console.log(array);
        lessonsCheckBox.each(function () {
            $(this).prop("checked", ($.inArray($(this).val(), array) != -1));
        });
    }

    function uncheckAnyBox(element) {
        element.each(function() {
			this.checked = false;
		});
    }

    function getCityByProvinceID(provinceID) {
        console.log('Fetching Cities with province ID '+provinceID);
        var url = '/educcato/students/getCitiesByProvinceID/'+provinceID;
        console.log(url);
        $.ajax(url)
        .done(function (response) {
            var cities = JSON.parse(response);
            cityOptions.empty();
            for (var i = 0; i < cities.length; i++) {
                cityOptions.append('<option value=' + cities[i].id + '>' + cities[i].name + '</option>');
            }
        })
        .fail(function (xhr) {
            alert('Error: ' + xhr.responseText);
        });
    }

});
