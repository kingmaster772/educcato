<?php

namespace Educcato;

use SilverStripe\Admin\ModelAdmin;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\ORM\ArrayLib;
use SilverStripe\Forms\TabSet;
use SilverStripe\Versioned\Versioned;

class ClassroomAdmin extends ModelAdmin {

    private static $menu_title = 'Classroom';

    private static $url_segment = 'classroom';

    private static $managed_models = [
        ClassroomData::class,
    ];

    public function getCMSfields() {
        $fields = FieldList::create(TabSet::create('Root'));
        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Name'),
            TextareaField::create('Description'),
        ]);

        return $fields;
    }

    public function searchableFields() {
        return [
            'Name' => [
                'filter' => 'PartialMatchFilter',
                'title' => 'Name',
                'field' => TextField::class,
            ],
        ];
    }
}
