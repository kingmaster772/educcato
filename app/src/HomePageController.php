<?php

namespace Educcato;

use PageController;

class HomePageController extends PageController {
    public function Students()  {
        $students = StudentsData::get();

        return $students;
    }
}
