<?php

namespace Educcato;

use SilverStripe\ORM\DataObject;
use SilverStripe\Versioned\Versioned;
use SilverStripe\Dev\Debug;

class StudentLessonData extends DataObject {
    private static $table_name = 'Student_Lesson';

    private static $db = [
        'Title' => 'Varchar',
    ];

    private static $has_one = [
        'Student' => StudentData::class,
        'Lesson' => LessonData::class,
    ];
}
