<?php

namespace Educcato;

use SilverStripe\ORM\DataObject;
use SilverStripe\Versioned\Versioned;
use SilverStripe\Dev\Debug;

class ProvinceData extends DataObject {


    private static $allowed_actions = [
        'getProvinces',
    ];

    private static $table_name = 'Province';

    private static $db = [
        'Name' => 'Varchar(100)',
    ];

    private static $has_many = [
        'City' => CityData::class,
    ];

    private static $versioned_gridfield_extensions = true;

    private static $extensions = [
        Versioned::class,
    ];

    private static $summary_fields = [
        'Name' => 'Name',
    ];

    private static $searchable_fields = [
        'Name',
    ];
}
