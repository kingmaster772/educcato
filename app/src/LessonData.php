<?php

namespace Educcato;

use SilverStripe\ORM\DataObject;

class LessonData extends DataObject {
    private static $table_name = 'Lesson';

    private static $db = [
        'Title' => 'Varchar(150)',
        'Description' => 'Text',
    ];

    private static $belongs_many_many = [
        'Students' => StudentData::class,
    ];

    private static $menu_icon = 'font-icon-book';
}
