<?php

namespace Educcato;

use SilverStripe\Admin\ModelAdmin;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\DateField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\CheckboxSetField;
use SilverStripe\Forms\CurrencyField;
use SilverStripe\ORM\ArrayLib;
use SilverStripe\Forms\TabSet;
use SilverStripe\Versioned\Versioned;

class ProvinceAdmin extends ModelAdmin {

    private static $menu_title = 'Province';

    private static $url_segment = 'province';

    private static $managed_models = [
        ProvinceData::class,
    ];

    public function getCMSfields() {
        $fields = FieldList::create(TabSet::create('Root'));
        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Name'),
        ]);
        // $fields->addFieldToTab('Root.Main', DropdownField::create(
        //     'ClassroomID',
        //     'Classroom',
        //     ClassroomData::get()->map('ID','Name')
        // )->setEmptyString('-- None --'), 'Content');

        return $fields;
    }

    public function searchableFields() {
        return [
            'Name' => [
                'filter' => 'PartialMatchFilter',
                'title' => 'Name',
                'field' => TextField::class,
            ],
        ];
    }
}
