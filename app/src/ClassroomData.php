<?php

namespace Educcato;

use SilverStripe\ORM\DataObject;
use SilverStripe\Versioned\Versioned;

class ClassroomData extends DataObject {
    private static $table_name = 'Classroom';

    private static $db = [
        'Name' => 'Varchar(150)',
        'Description' => 'Text',
    ];

    private static $has_many = [
        'Students' => StudentData::class,
    ];

    private static $versioned_gridfield_extensions = true;

    private static $menu_icon = 'font-icon-home';

    private static $extensions = [
        Versioned::class,
    ];

    private static $summary_fields = [
        'Name',
        'studentsCount' => 'Jumlah Siswa',
    ];

    private static $searchable_fields = [
        'Name',
    ];

    public function studentsCount() {
        return count($this->Students());
    }
}
