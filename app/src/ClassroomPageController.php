<?php

namespace Educcato;

use PageController;

class ClassroomPageController extends PageController {

    public function Classrooms() {
        $classrooms = ClassroomData::get();
        return $classrooms;
    }

}
