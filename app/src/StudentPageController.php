<?php

namespace Educcato;

use PageController;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\Form;
use SilverStripe\Forms\FormAction;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\CheckboxSetField;
use SilverStripe\Forms\HiddenField;
use SilverStripe\Dev\Debug;
use SilverStripe\ORM\Queries\SQLDelete;
use SilverStripe\ORM\PaginatedList;
use SilverStripe\ORM\ArrayLib;
use SilverStripe\ORM\ArrayList;
use SilverStripe\View\ArrayData;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Control\HTTP;
use SilverStripe\Control\Email\Email;
use Monolog\Formatter\JsonFormatter;

class StudentPageController extends PageController {

    private $form;
    private $errorMsg = [
        'Name' => 'Nama terlalu pendek!',
        'Phone' => 'No. HP terlalu pendek!',
        'Address' => 'Alamat terlalu pendek!'
    ];

    private static $allowed_actions = [
        'new',
        'edit',
        'update',
        'delete',
        'RenderForm',
        'RenderSearchForm',
        'test',
        'getStudents',
        'getProvinces',
        'getCitiesByProvinceID',
    ];

    public function test() {
        die('it works');
    }

    public function index(HTTPRequest $request) {
        $students = StudentData::get()->sort('LastEdited', 'DESC');

        if ($search = $request->getVar('Keywords')) {
            $students = $students->filterAny([
                'Name:PartialMatch' => $search,
                'Phone:PartialMatch' => $search
            ]);
        }

        $paginatedStudents = PaginatedList::create(
            $students,
            $request
        )
            ->setPageLength(5)
            ->setPaginationGetVar('s');

        $data = [
            'Results' => $paginatedStudents
        ];

        if($request->isAjax()) {
            return $this->customise($data)
                            ->renderWith('Educcato/Includes/StudentSearchResults');
        }

        return $data;
    }

    public function RenderSearchForm() {
        $this->form = Form::create(
            $this,
            'StudentSearchForm',
            FieldList::create(
                TextField::create('Keywords')
                    ->setAttribute('placeholder', 'Name, Phone, ....')
                    ->addExtraClass('form-control'),
            ),
            FieldList::create(
                FormAction::create('handleSearch','Search')
                    ->addExtraClass('btn-lg btn-fullcolor')
            )
        );

        $this->form->setFormMethod('GET')
            ->setFormAction($this->Link())
            ->disableSecurityToken()
            ->loadDataFrom($this->request->getVars());;

        return $this->form;
    }

    public function RenderForm() {
        $fields = new FieldList(
            HiddenField::create('ID',''),
            HiddenField::create('State','')
                ->setAttribute('value','insert'),
            TextField::create('Name', 'Nama')
                ->setAttribute('placeholder','John Smith'),
            TextField::create('Phone', 'No. HP')
                ->setAttribute('placeholder','8977219772 atau 857373422'),
            DropdownField::create(
                'ClassroomID',
                'Pilih Kelas',
                ClassroomData::get()->map('ID','Name')),
            CheckboxSetField::create(
                'Lessons',
                'Pilih Mata Pelajaran',
                LessonData::get()->map('ID','Title')),
            DropdownField::create(
                'ProvinceID',
                'Provinsi',
                ProvinceData::get()->map('ID','Name')),
            DropdownField::create(
                'CityID',
                'Kota',
                CityData::get()->map('ID','Name')),
            TextareaField::create('Address', 'Alamat')
                ->setAttribute('placeholder','Jl. Sesama III/24'),
        );

        $actions = new FieldList(
            FormAction::create('handleForm')->setTitle('Save')
        );

        $required = new RequiredFields('Name');

        $this->form = new Form($this, 'RenderForm', $fields, $actions, $required);

        return $this->form;
    }

    public function getStudents() {
        $students = StudentData::get()->sort('LastEdited', 'DESC');
        return $students;
    }

    public function handleForm($data, Form $form) {
        $this->form = $form;
        $state = $data['State'];

        switch ($state) {
            case 'insert':
                $this->new($data, $this->form);
            break;
            case 'update':
                $this->update();
            break;
            default:
                $this->new($data, $this->form);
            break;
        }
    }

    public function new($data, Form $form) {
        // Debug::show($data);
        // die();

        $this->form = $form;
        $name = $data['Name'];
        $phone = $data['Phone'];
        $address = $data['Address'];
        $classroom = $data['ClassroomID'];
        $city = $data['CityID'];
        $lessons = $data['Lessons'];

        if ($this->validateLength($name, 3)) {
            $this->form->sessionMessage($this->errorMsg['Name'],'bad');
            return $this->redirectBack();
        }

        if ($this->validateLength($phone, 9)) {
            $this->form->sessionMessage($this->errorMsg['Phone'],'bad');
            return $this->redirectBack();
        }

        if ($this->validateLength($address, 10)) {
            $this->form->sessionMessage($this->errorMsg['Address'],'bad');
            return $this->redirectBack();
        }

        $student = StudentData::create();
        $student->Name = $name;
        $student->Phone = $phone;
        $student->Address = $address;
        $student->ClassroomID = $classroom;
        $student->CityID = $city;
        $student->write();

        foreach ($lessons as $lesson) {
            // var_dump($lesson);
            $student->Lessons()->add($lesson);
        }

        $from = 'no-replay@example.com';
        $to = 'agilsetiawan772@gmail.com';
        $subject = 'Log Report - Add Student';
        $this->sendMail($from, $to, $subject, $student);
        // var_dump($this->sendMail($student));
        // die();

        // $studentLessons =
        // print_r($data);

        // Using the Form instance you can get / set status such as error messages.
        // $form->sessionMessage('Successful!', 'good');

        // After dealing with the data you can redirect the user back.
        // return $this->redirectBack();
    }

    public function edit() {
        $id = $this->request->param('ID');
        $student = StudentData::create(['ID' => $id]);
        $student->toJSONArray();
        Debug::show($student);
    }

    public function update() {
        $request = $this->request;
        $id = $request['ID'];
        $name = $request['Name'];
        $phone = $request['Phone'];
        $address = $request['Address'];
        $classroom = $request['ClassroomID'];
        $city = $request['CityID'];
        $lessons = $request['Lessons'];

        // var_dump($this->validateLength($name, 3, $this->errorMsg['Name']));
        // die();

        // $this->validateLength($phone, 9, $this->errorMsg['Phone']);
        // $this->validateLength($address, 10, $this->errorMsg['Address']);

        if ($this->validateLength($name, 3)) {
            $this->form->sessionMessage($this->errorMsg['Name'],'bad');
            return $this->redirectBack();
        }

        if ($this->validateLength($phone, 9)) {
            $this->form->sessionMessage($this->errorMsg['Phone'],'bad');
            return $this->redirectBack();
        }

        if ($this->validateLength($address, 10)) {
            $this->form->sessionMessage($this->errorMsg['Address'],'bad');
            return $this->redirectBack();
        }

        $student = StudentData::create(['ID' => $id]);
        $student->Name = $name;
        $student->Phone = $phone;
        $student->Address = $address;
        $student->ClassroomID = $classroom;
        $student->CityID = $city;
        $student->write();

        foreach ($lessons as $lesson) {
            // var_dump($lesson);
            $student->Lessons()->add($lesson);
        }

        $from = 'no-replay@example.com';
        $to = 'agilsetiawan772@gmail.com';
        $subject = 'Log Report - Update Student';
        $this->sendMail($from, $to, $subject, $student);

        // return $this->redirectBack();
    }

    public function delete() {
        $id = $this->request->param('ID');
        $student = StudentData::create(['ID' => $id]);
        // echo "ID = " . $id;
        // echo "<br>Name: " . $student->Name;
        $name = $student->Name;
        $student->delete();

        // After dealing with the data you can redirect the user back.
        return $this->redirectBack();
    }

    public function validateLength($field, $maxLength) {
        /*
        if(strlen($field) < $maxLength) {
            $this->form->sessionMessage($message,'bad');
            // Debug::show('len: '.strlen($field));
        }
        return $this->redirectBack();
        */
        return (strlen($field) > 0 && strlen($field) < $maxLength);
    }

    public function showError($validate, $message) {
        if ($validate) {
            $this->form->sessionMessage($message,'bad');
            return $this->redirectBack();
        }
    }

    public function sendMail($from, $to, $subject, $data = null) {
        // $this->logger->log(LogLevel::ERROR, '"'.$body.'"');
        /*
        $body = '
            Siswa Baru <br>
            - Nama: '.$student->Name.' <br>
            - No. HP: '.$student->Phone.' <br>
            - Kelas: '.$student->Classroom->Name.' <br>
            - Mapel yang sedang diambil: <br>
        ';
        $email = new Email($from, $to, $subject, $body);
        $email->sendPlain();
        */

        $email = Email::create()
            ->setHTMLTemplate('Email\\StudentLogEmail')
            ->setData([
                'Student' => $data
            ])
            ->setFrom($from)
            ->setTo($to)
            ->setSubject($subject);

        $email->send();

        $arrayData = new ArrayData([
            'SenderEmail' => $from,
            'ReceipentEmail' => $to,
            'Student' => $data
        ]);

        echo $arrayData->renderWith('Educcato\Includes\EmailSentPage');
    }

    public function getProvinces() {
        $jsonFormatter = new JsonFormatter();

        $provinces = ProvinceData::get();
        // print_r($provinces);
        $array = array();
        $i = 0;
        foreach ($provinces as $province) {
            $array[$i] = $province->Name;
            $i++;
            // echo $province->Name . "<br>";
        }
        print_r($array);
        // Debug::show($provinces->Name);
        // $this->response->addHeader('Content-Type', 'application/json');
        // return json_encode($provinces);
    }

    public function getCitiesByProvinceID(HTTPRequest $request) {

        // var_dump($request->allParams()['ID']);
        // die();
        $cities = CityData::get()->filter([
            'ProvinceID' => (int)$request->allParams()['ID']
        ]);
        // Debug::show($cities);
        $array = array();
        $i = 0;
        foreach ($cities as $city) {
            $array[$i]['id'] = $city->ID;
            $array[$i]['name'] = $city->Name;
            $i++;
        }
        return json_encode($array);
    }

}
