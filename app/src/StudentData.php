<?php

namespace Educcato;

use SilverStripe\ORM\DataObject;
use SilverStripe\Versioned\Versioned;
use SilverStripe\Dev\Debug;

class StudentData extends DataObject {


    private static $allowed_actions = [
        'LessonsList',
    ];
    private static $table_name = 'Student';

    private static $db = [
        'Name' => 'Varchar(150)',
        'Phone' => 'Varchar(20)',
        'Address' => 'Text',
        'FeaturedOnHomepage' => 'Boolean',
    ];

    private static $has_one = [
        'Classroom' => ClassroomData::class,
        'City' => CityData::class,
    ];

    private static $many_many = [
        'Lessons' => LessonData::class,
    ];

    private static $versioned_gridfield_extensions = true;

    private static $menu_icon = 'font-icon-torsos-all';

    private static $extensions = [
        Versioned::class,
    ];

    private static $summary_fields = [
        'Name' => 'Name',
        'Phone' => 'Phone',
        'Classroom.Name' => 'Classroom',
        'FeaturedOnHomepage.Nice' => 'Featured?'
    ];

    private static $searchable_fields = [
        'Name',
        'Phone',
        'Classroom.Name',
        'City.Name',
        'FeaturedOnHomepage'
    ];

    public function onBeforeWrite() {
        parent::onBeforeWrite();

        if (preg_match('/[^A-Za-z?! ]/', $this->Name)) {
            $this->Name = preg_replace('/[^A-Za-z?! ]/', '', $this->Name);
        }

        if (preg_match('/\D/', $this->Phone)) {
            $this->Phone = preg_replace('/\D/', '', $this->Phone);
        }

        if (empty($this->Phone) || (preg_match('/\s/',$this->Phone) > 0)) {
            $this->Phone = '08XXX';
        }

        if (substr($this->Phone, 0, 3) == '+62') {
            $this->Phone = str_replace('+62', '0', $this->Phone);
        }

        if (substr($this->Phone, 0, 2) == '62') {
            $this->Phone = str_replace('62', '0', $this->Phone);
        }

        if (empty($this->Address)) {
            $this->Address = 'Jalan Sesama';
        }
    }

    public function onAfterWrite() {
        parent::onAfterWrite();
    }

    public function LessonsList() {
        if($this->Lessons()->exists()) {
            return implode(', ', $this->Lessons()->column('ID'));
        }

        return '0';
    }

    public function toJSONArray() {
        $student = array();
        $student['Name'] = $this->Name;
        return $student;
    }
}
