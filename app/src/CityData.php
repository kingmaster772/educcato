<?php

namespace Educcato;

use SilverStripe\ORM\DataObject;
use SilverStripe\Versioned\Versioned;
use SilverStripe\Dev\Debug;

class CityData extends DataObject {


    private static $allowed_actions = [
        '',
    ];

    private static $table_name = 'City';

    private static $db = [
        'Name' => 'Varchar(100)',
        'PostalCode' => 'Varchar(10)',
    ];

    private static $has_one = [
        'Province' => ProvinceData::class,
    ];

    private static $has_many = [
        'Student' => StudentData::class,
    ];

    private static $versioned_gridfield_extensions = true;

    private static $extensions = [
        Versioned::class,
    ];

    private static $summary_fields = [
        'Name' => 'Nama Kota',
        'PostalCode' => 'Kode Pos',
        'Province.Name' => 'Provinsi',
    ];

    private static $searchable_fields = [
        'Name',
        'PostalCode',
    ];
}
