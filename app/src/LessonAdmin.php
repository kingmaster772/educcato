<?php

namespace Educcato;

use SilverStripe\Admin\ModelAdmin;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\DateField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\CurrencyField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\ORM\ArrayLib;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\TabSet;
use SilverStripe\Versioned\Versioned;

class LessonAdmin extends ModelAdmin {

    private static $menu_title = 'Lesson';

    private static $url_segment = 'lesson';

    private static $managed_models = [
        LessonData::class,
    ];

    private static $extensions = [
        Versioned::class,
    ];

    private static $summary_fields = [
        'Title' => 'Title',
        'Description' => 'Description',
    ];

    private static $searchable_fields = [
        'Title',
        'Description',
    ];

    public function getCMSfields() {
        $fields = FieldList::create(TabSet::create('Root'));
        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Title'),
            TextareaField::create('Description'),
        ]);

        return $fields;
    }

    public function searchableFields() {
        return [
            'Title' => [
                'filter' => 'PartialMatchFilter',
                'title' => 'Title',
                'field' => TextField::class,
            ],
            'Description' => [
                'filter' => 'PartialMatchFilter',
                'title' => 'Description',
                'field' => TextareaField::class,
            ],
        ];
    }
}
