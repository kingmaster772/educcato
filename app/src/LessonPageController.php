<?php

namespace Educcato;

use PageController;

class LessonPageController extends PageController {

    public function Lessons() {
        $lessons = LessonData::get();
        return $lessons;
    }

}
