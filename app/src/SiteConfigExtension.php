<?php

namespace Educcato;

use SilverStripe\ORM\DataExtension;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Assets\File;
use SilverStripe\AssetAdmin\Forms\UploadField;

class SiteConfigExtension extends DataExtension {

    private static $db = [
        'FacebookLink' => 'Varchar',
        'TwitterLink' => 'Varchar',
        'GoogleLink' => 'Varchar',
        'YouTubeLink' => 'Varchar',
        'FooterContent' => 'Text'
    ];

    private static $has_one = [
        'Favicon' => File::class,
    ];

    private static $owns = [
        'Favicon',
    ];

    public function updateCMSFields(FieldList $fields) {
        $fields->addFieldsToTab('Root.Social', array (
            TextField::create('FacebookLink','Facebook'),
            TextField::create('TwitterLink','Twitter'),
            TextField::create('GoogleLink','Google'),
            TextField::create('YouTubeLink','YouTube')
        ));

        $fields->addFieldToTab('Root.Meta', $favicon = UploadField::create('Favicon','Favicon web, optional (.ico only)'));
        $favicon->setFolderName('web-favicon');
        $favicon->getValidator()->setAllowedExtensions(array('ico'));

        $fields->addFieldsToTab('Root.Main', TextareaField::create('FooterContent', 'Content for footer'));
    }

    public function onAfterWrite() {
        parent::onAfterWrite();
        if ($this->Favicon()->exists() && !$this->Favicon()->isPublished()) {
            $this->Favicon()->doPublish();
        }
    }
}
