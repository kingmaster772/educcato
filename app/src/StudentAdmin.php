<?php

namespace Educcato;

use SilverStripe\Admin\ModelAdmin;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\DateField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\CheckboxSetField;
use SilverStripe\Forms\CurrencyField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\ORM\ArrayLib;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\TabSet;
use SilverStripe\Versioned\Versioned;

class StudentAdmin extends ModelAdmin {

    private static $menu_title = 'Student';

    private static $url_segment = 'student';

    private static $managed_models = [
        StudentData::class,
    ];

    public function getCMSfields() {
        $fields = FieldList::create(TabSet::create('Root'));
        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Name'),
            TextField::create('Phone'),
            CheckboxField::create('FeaturedOnHomepage','Feature on homepage'),
        ]);
        $fields->addFieldToTab('Root.Address', DropdownField::create(
            'CityID',
            'City',
            CityData::get()->map('ID','Name')
        )->setEmptyString('-- None --'), 'Content');
        $fields->addFieldToTab('Root.Main', DropdownField::create(
            'ClassroomID',
            'Classroom',
            ClassroomData::get()->map('ID','Name')
        )->setEmptyString('-- None --'), 'Content');
        $fields->addFieldToTab('Root.Lessons', CheckboxSetField::create(
            'Lessons',
            'Selected Lessons',
            $this->Parent()->Lessons()->map('ID','Title')
        ));

        return $fields;
    }

    public function searchableFields() {
        return [
            'Name' => [
                'filter' => 'PartialMatchFilter',
                'title' => 'Name',
                'field' => TextField::class,
            ],
            'Phone' => [
                'filter' => 'PartialMatchFilter',
                'title' => 'Phone',
                'field' => TextField::class,
            ],
            'FeaturedOnHomepage' => [
                'filter' => 'ExactMatchFilter',
                'title' => 'Only featured'
            ]
        ];
    }
}
