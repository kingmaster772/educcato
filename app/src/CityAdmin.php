<?php

namespace Educcato;

use SilverStripe\Admin\ModelAdmin;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\DateField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\CheckboxSetField;
use SilverStripe\Forms\CurrencyField;
use SilverStripe\ORM\ArrayLib;
use SilverStripe\Forms\TabSet;
use SilverStripe\Versioned\Versioned;

class CityAdmin extends ModelAdmin {

    private static $menu_title = 'City';

    private static $url_segment = 'city';

    private static $managed_models = [
        CityData::class,
    ];

    public function getCMSfields() {
        $fields = FieldList::create(TabSet::create('Root'));
        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Name'),
            TextField::create('PostalCode'),
        ]);
        $fields->addFieldToTab('Root.Main', DropdownField::create(
            'ProvinceID',
            'Province',
            ProvinceData::get()->map('ID','Name')
        )->setEmptyString('-- None --'), 'Content');

        return $fields;
    }

    public function searchableFields() {
        return [
            'Name' => [
                'filter' => 'PartialMatchFilter',
                'title' => 'Nama Kota',
                'field' => TextField::class,
            ],
            'PostalCode' => [
                'filter' => 'PartialMatchFilter',
                'title' => 'Kode Pos',
                'field' => TextField::class,
            ],
        ];
    }
}
