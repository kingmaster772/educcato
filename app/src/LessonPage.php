<?php

namespace Educcato;

use Page;

class LessonPage extends Page {
    public function Lessons() {
        $lessons = LessonData::get();
        return $lessons;
    }
}
