<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    Siswa Baru<br>
    - Nama: $Student.Name <br>
    - No. HP: $Student.Phone <br>
    - Kelas: $Student.Classroom.Name <br>
    <% if Student.Lessons %>
        - Mapel yang sedang diambil: <br>
        <ul>
        <% loop $Student.Lessons %>
            <li>$Title</li>
        <% end_loop %>
        </ul>
    <% end_if %>
</body>
</html>
